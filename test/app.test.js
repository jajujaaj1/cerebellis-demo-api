const request = require("supertest");

const app = require("../src/app");

describe("app", () => {
  it("responds with a not found message", (done) => {
    request(app)
      .get("/what-is")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(404, done);
  });
});

describe("GET /", () => {
  it("responds with a json message", (done) => {
    request(app)
      .get("/")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(
        200,
        {
          message: "Cerebellis Demo Api ",
          description:
            "Cerebellis demo api for the description purposes of job application. Current file set it public and it is for the purpose of explaining the coding experience in terms of skill set and project procedures.",
        },
        done
      );
  });
});
