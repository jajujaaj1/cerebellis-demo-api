const request = require("supertest");

const app = require("../src/app");

describe("GET /api/v1/users", () => {
  it("responds with a json objects", (done) => {
    request(app)
      .get("/api/v1/users")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});

describe("POST /api/v1/users/6083140698cc142f18976ea1", () => {
  it("responds with a json objects", (done) => {
    request(app)
      .get("/api/v1/users/")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
