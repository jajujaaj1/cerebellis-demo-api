const express = require("express");

const emojis = require("./emojis");
const users = require("./users");

const router = express.Router();

router.get("/", (req, res) => {
  res.json({
    message: "API - Version 1.0.0",
  });
});

router.use("/users", users);
router.use("/emojis", emojis);

module.exports = router;
