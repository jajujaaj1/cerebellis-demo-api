const express = require("express");

const router = express.Router();
const User = require("./userModel");

/**
 * GET: /
 * description: Return all users
 * responses:
 *    200: Successful
 *    404: Not found
 */
router.get("/", async (req, res) => {
  try {
    const usersData = await User.find({});
    res.status(200).send({ message: "List of all users", users: usersData });
  } catch (error) {
    res.status(400).send({
      message: "Unable to process request to find all users",
      err: err,
    });
  }
});

/**
 * GET: /:id
 * description: Return user based on id
 * responses:
 *    200: Successful
 *    404: Not found
 */
router.get("/:id", async (req, res) => {
  try {
    const userData = await User.findById(req.params.id, "email");
    res.status(200).send({
      status: "Succesful",
      message: "Detail of user",
      users: userData,
    });
  } catch (error) {
    res.status(200).send({
      status: "Unsuccessful",
      message: `Unable to find user by id ${req.params.id}`,
      error: error,
    });
  }
});

/**
 * POST: /
 * description: Add user to database
 * responses:
 *    200: Successful
 *    404: Not found
 *    500: Server not able to process request
 */
router.post("/", (req, res) => {
  const userData = new User(req.body);
  userData
    .save()
    .then((item) => {
      res
        .status(200)
        .send({ message: "User created successfully", user: item });
    })
    .catch((err) => {
      res
        .status(400)
        .send({ message: "Unable to save user to storage", err: err });
    });
});

module.exports = router;
