const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const cors = require("cors");
const connectDB = require("./config/db");
const bodyParser = require("body-parser");

require("dotenv").config();

//Connect to Database
connectDB();

const middlewares = require("./middlewares");
const api = require("./api");

const app = express();

app.use(morgan("dev"));
app.use(helmet());
app.use(cors());
app.use(express.json());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.json({
    message: "Cerebellis Demo Api ",
    description:
      "Cerebellis demo api for the description purposes of job application. Current file set it public and it is for the purpose of explaining the coding experience in terms of skill set and project procedures.",
  });
});

app.get("/api", (req, res) => {
  res.json({
    message: "Lists all versions of API",
    description: {
      version: "1.0.0",
      designedBy: "Muhammad Junaid Iftikhar",
      active: "true",
    },
  });
});

app.use("/api/v1", api);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
